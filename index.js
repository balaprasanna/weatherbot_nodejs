'use strict';
 
const {WebhookClient} = require('dialogflow-fulfillment');
const {Card, Suggestion} = require('dialogflow-fulfillment');
var requests = require('request');

var express = require('express');
var port = process.env.PORT || 3000;
var app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());


app.post("/", function (request, response) {
   // res.send(JSON.stringify({ Hello: ‘World’}));

  const agent = new WebhookClient({ request, response });
  console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
  console.log('Dialogflow Request body: ' + JSON.stringify(request.body));
 
  function getWeatherIntentHandler(agent) {
    let location = request.body["queryResult"]["parameters"]["location"]
    
    var API_ENDPOINT = `http://api.openweathermap.org/data/2.5/weather?APPID=8a81d247d650cb16469c4ba3ceb7d265&q=${location}`
    
    return new Promise((resolve, reject) => { 

      requests(API_ENDPOINT, function (error, response, body) {
            console.log('error:', error); console.log('body:', body); 

            if (error) {
              agent.add(`Something went wrong`);
              reject()
            }

            body = JSON.parse(body)
            let code = body["cod"]
            
            if (code === 200) {
                let info = body["weather"][0]["description"]
                agent.add(`Currently in ${location} , its ${info}`);
              }
            else {
              agent.add(` ${location} City Not found , Please check the city name`);
            }
            
            resolve()

          });
    })
    
    
  }
 
  // Run the proper function handler based on the matched Dialogflow intent name
  let intentMap = new Map();

  intentMap.set('GetWeatherIntent', getWeatherIntentHandler);
 
  agent.handleRequest(intentMap);

});

app.listen(port, function () {
 console.log(`NodeJS Backend is listening on port !`, port);
});